<?php
include "../config.php";
// Connexion à la base de données

require '../vendor/autoload.php';

$client = new MongoDB\Client("mongodb://${username}:${pwd}@localhost:27017/");
$collection = $client->FlickrSearch->photos;
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>FlickrSearch</title>
</head>
<body>
  <?php
  if (isset($_GET['gallery'])) {
    $gallery = $_GET['gallery'];
  }
  else{
    $gallery = false;
  }

  if (isset($_GET['safe_search'])) {
    $safeSearch = $_GET['safe_search'];
  }
  else{
    $safeSearch = false;
  }

  // On remplace les espaces par l'encodage URL des virgules ","
  $tags = str_replace(" ", "%2C+", $_GET['tags']);
  $dateMinimum = strtotime($_GET['date-minimum-upload']);
  $dateMaximum = strtotime($_GET['date-maximum-upload']);

  if ($dateMaximum != ""){
      $dateMaximum += 86399;
  }

  // On regarde si la BDD a des images avec ces tags
  // On utilise une Regex pour pouvoir
  //$regex = new MongoDB\BSON\Regex("/".$tags."/i");
  $result = $collection->find(['tags' => $tags])->toArray();

  // Si il n'y a aucun résultat on demande à l'API
  if (count($result) == 0) {
      print_r("Requesting API !");

    $url = "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=".$api_key."&tags=".$tags."&min_upload_date=".$dateMinimum."&max_upload_date=".$dateMaximum."&safe_search=".$safeSearch."&in_gallery=".$gallery."&format=json&nojsoncallback=1";
    $json_text = json_decode(file_get_contents($url), true);
    $photos = $json_text['photos']['photo'];



    foreach ($photos as $photo) {
      $results = $collection->find(['id' => $photo['id']]);

      if (count($result) > 0) {
        $tags .= $results['tags'];
        $collection->updateOne( ['id' => $photo['id']], ['$set' => ['tags' => $tags]]);
      }
      else {
        $photo['tags'] = $tags;
        $photo['safe_search'] = $safeSearch;
        $photo['dateMinimum'] = $dateMinimum;
        $photo['dateMaximum'] = $dateMaximum;
        $photo['gallery'] = $gallery;
        $collection->insertOne($photo);
      }

    }
  } else {
      print_r("Requesting Database !");
      $photos = $result;
  }

  // Enfin on affiche les photos
  foreach ($photos as $photo) {
    $farm_id = $photo['farm'];
    $server_id = $photo['server'];
    $id = $photo['id'];
    $secret = $photo['secret'];
    $title = $photo['title'];
    $size = $_GET['size-image'];
    $img_url = "https://farm" . $farm_id . ".staticflickr.com/" . $server_id . "/" . $id . "_" . $secret . $size. ".png";
  ?>
      <a href="../details/details.php?idPhoto=<?php echo $id ?>">
          <img src="<?php echo $img_url ?>"  alt="<?php echo $title ?>" title="<?php echo $title ?>"/>
      </a>
  <?php
}
?>
</body>
</html>
