<?php
include "../config.php";
// Connexion à la base de données

require '../vendor/autoload.php';
$idPhoto = $_GET["idPhoto"];

$client = new MongoDB\Client("mongodb://${username}:${pwd}@localhost:27017/");
$collection = $client->FlickrSearch->details;
$photo = $client->FlickrSearch->photos->findOne(["id" => $idPhoto]);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>FlickrSearch</title>
</head>
<body>

</body>
</html>
<?php
$comments = $collection->findOne(["photo_id" => $idPhoto]);
if (isset($comments)) {
    print_r("Requesting Database for comments ! \r");
}
else {
    print_r("Requesting API for comments ! \r");
    $url = "https://www.flickr.com/services/rest/?method=flickr.photos.comments.getList&api_key=".$api_key."&photo_id=".$idPhoto."&format=json&nojsoncallback=1";
    $json_text = json_decode(file_get_contents($url), true);

    if (isset($json_text['comments']['comment'])) {
        $comments = $json_text['comments']['comment'];
        foreach ($comments as $comment) {
            $collection->insertOne($comment);
        }
    }
}

$farm_id = $photo['farm'];
$server_id = $photo['server'];
$id = $photo['id'];
$secret = $photo['secret'];
$title = $photo['title'];
$owner = $photo['owner'];
$img_url = "https://farm" . $farm_id . ".staticflickr.com/" . $server_id . "/" . $id . "_" . $secret .  ".png";

if (strpos($owner, "@")) {
    print_r("Requesting API for owner name ! \r");
    $owner_id = $photo['owner'];
    $url = "https://www.flickr.com/services/rest/?method=flickr.people.getInfo&api_key=". $api_key . "&user_id=" . $owner_id . "&format=json&nojsoncallback=1";
    $json_text = json_decode(file_get_contents($url), true);
    $owner = $json_text['person']['username']['_content'];

    $client->FlickrSearch->photos->updateOne(['id' => $idPhoto], ['$set' => ['owner' => $owner]]);
}
else {
    print_r("Requesting Database for owner name ! \r");
}
?>

<div id="main_wrapper">
    <div id="card">
        <img src="<?php echo $img_url ?>"  alt="<?php echo $title ?>" title="<?php echo $title ?>"/>
        <div id="details">
            <h3>Posteur :</h3>
            <p><?php echo $owner ?></p>

            <h3>Commentaires :</h3>
            <?php
            if (isset($comments)) {
                foreach ($comments as $comment) { ?>
                    <p>
                        <strong><?php echo $comment['authorname']?></strong> <?php echo $comment['_content'] ?>
                    </p>
                <?php }
            }
            else {
                echo "Il n'y a aucun commentaires";
            }
            ?>
        </div>
    </div>
</div>
</body>
</html>
